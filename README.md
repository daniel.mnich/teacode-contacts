This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Setup

### clone the repository to your local machine
    `git clone`
### open the terminal in your teacode-contacts folder
### run `npm install` to install all necessary packages

### run `npm start` to launch the app

## App features

The app fetches contacts from the API provided by TeaCode;
The user has the options to:
* seach for specific contacts by name, surname or the combiantion of those (ex. <name>/<surname>/<name surname>)
* add/remove contacts to selected list which displays all the selected contacts IDs' 


## Have fun checking it out!
