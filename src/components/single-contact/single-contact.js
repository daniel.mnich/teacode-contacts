import React from 'react';
import './single-contact.css';

function SingleContact(props) {
	return (
		<div className="singleContact-container" >
			<div className="img-container mr-1">
				{ props.contactInfo.avatar ? 
				<img src={props.contactInfo.avatar} alt="This is your contacts avatar" /> : 
				<div className="singleContact-initials">
					<span>
						{props.contactInfo.first_name[0]}{props.contactInfo.last_name[0]}
					</span>
				</div>
			 	}
			</div>
		 	<div className="userInfo-container mr-1">
				<div className="userInfo-name">
					{`${props.contactInfo.first_name} ${props.contactInfo.last_name}`}
				</div>
				<div className="userInfo-email">
					{props.contactInfo.email}
				</div>
			</div>	
			<div className="checkbox-container">
				<input
				name="isGoing"
				type="checkbox"
				defaultChecked={props.contactInfo.isChecked} />
			</div>
    </div>
  );
}

export default SingleContact;
