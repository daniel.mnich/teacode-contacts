import React from 'react';
import SingleContact from './single-contact/single-contact';
import './Main.css';

class Main extends React.Component {
	constructor(props) {
		super(props)
		this.handleChange = this.handleChange.bind(this);
	}
	state = {
		contacts: [],
		selectedContacts: [],
		downloadLink: 'https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json',
		searchedString: ''
	};
	
	componentDidMount() {
		this.getContacts(this.state.downloadLink);
	}
	
	// fetches {}[] from API , sorts it and assigns it to state variable
	async getContacts (url) {
		await fetch(url, {
			mode: 'cors'
		})
		.then(response => {
			if(response) {
				response.json().then(async result => {
					return this.setState({
						contacts: await this.sortContacts(result)
					})
				})
			}
		  });
	}
	
	// sorts {}[] by {}.last_name value
	sortContacts (arrToSort) {
		arrToSort.sort((a, b) => {
			return a.last_name < b.last_name ? -1 : a.last_name > b.last_name ? 1 : 0
		});
		return arrToSort;
	}
	
	//basic change handler in the app used only for the search input
	handleChange(event) {
		const name = event.target.name;
		const value = event.target.value;
		return this.setState({
			[name]: value
		})
	}

	// after the typing in 3 characters checks if the full name of given {} contact contains the searched value
	validateContactRender(contactName, contactSurname) {
		const normalizedName = contactName.toLowerCase();
		const normalizedSurname = contactSurname.toLowerCase();
		const normalizedFullame = normalizedName + ' ' + normalizedSurname;
		const normalizedSearchedString = this.state.searchedString.toLowerCase();
	 if (this.state.searchedString.length < 3 || normalizedFullame.indexOf(normalizedSearchedString) !== -1) {
			return true
		} else {
			return false
		}
	}
	
	// removes all contacts with the given id from the selected {}[] => with the assumption that all ids are unique
	removeContactFromSelected (id) {
		return this.setState({
			selectedContacts: this.state.selectedContacts.filter(obj => obj.id !== id)
		});
	}
	
	// adds provided {} to the selected {}[]
	addContactToSelected (obj) {
		return this.setState({
			selectedContacts: [...this.state.selectedContacts, obj]
		})
	}
	
	// changes {} isChecked property and adds / removes the given {} from seleceted {}[]
	toggleCheckbox(obj) {
		obj.isChecked ? this.removeContactFromSelected(obj.id) : this.addContactToSelected(obj);
		obj.isChecked = !obj.isChecked;
		return obj;
	}
	
	// toggles {} isChecked porperty and loggs selected {}[] 
	async onSingleContactClick (contact) {
		await this.toggleCheckbox(contact);
		this.logIds(this.state.selectedContacts);
		return 0;
	}
	
	// loggs selected {}[]
	logIds(arrOfObj) {
		console.log('CURRENTLY SELECTED DEVICES:')
		arrOfObj.forEach(obj => {
			console.log(obj.id)
		});
	}
	render () {	
		return (
			<div className="main-container">
				<header className="header">
					<h1>Contacts</h1>
				</header>
				<div className="main-input-container">
					<input className="main-input" name="searchedString" placeholder="Search..." 
							onChange={this.handleChange}/>
				</div>
				<ul className="contactList">
					{
						this.state.contacts.map(contact => {
							if (this.validateContactRender(contact.first_name, contact.last_name)) {
								return (
									<li key = {contact.id} 
										onClick={()=> this.onSingleContactClick(contact)}>
										<SingleContact 
														contactInfo = {contact}
														/>
									</li>
								)
							} else {
								return false
							}
						})
					}
				</ul>

			</div>
		)
	}
}

export default Main
